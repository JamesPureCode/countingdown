﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CountingDown.Models;

namespace CountingDown.ViewModels.Account
{
    public class UserViewModel
    {
        public UserViewModel()
        {
        }

        public UserViewModel(UserDetails user)
        {
            this.Email = user.Email;
            this.Name = user.Name;
        }

        [Key]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Your full name")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
