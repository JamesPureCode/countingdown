﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CountingDown.Models;

namespace CountingDown.ViewModels.Account
{
    public class ConfirmEventViewModel
    {
        public ConfirmEventViewModel()
        {
        }

        public ConfirmEventViewModel(UserDetails ownerDetails, EventDetails eventDetails)
        {
            this.Owner = new UserViewModel(ownerDetails);
            this.Event = new EventViewModel(eventDetails);
        }

        [System.ComponentModel.DataAnnotations.Key]
        public string Key { get; set; }

        public EventViewModel Event { get; }

        public UserViewModel Owner { get; }
    }
}
