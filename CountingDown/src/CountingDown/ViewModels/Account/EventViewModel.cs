﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using CountingDown.Models;
using System.Collections.Generic;
using Microsoft.AspNet.Mvc.Rendering;
using CountingDown.Services;
using System.IO;

namespace CountingDown.ViewModels.Account
{
    public class EventViewModel
    {
        private readonly List<string> _imageUrls = new List<string>();

        private static readonly List<string> _styles = new List<string>
        {
            "Default",
            "Cerulean",
            "Cosmo",
            "Cyborg",
            "Flatly",
            "Journal",
            "Simplex",
            "Spacelab",
            "Superhero",
            "Yeti"
        };

        public EventViewModel()
        {
            this.StyleName = "Default";
        }

        public EventViewModel(EventDetails details)
        {
            DateTime utcDue = DateTimeSupport.FromString(details.Due);

            this.AllowComments = details.AllowComments;
            this.Created = DateTimeSupport.FromString(details.Created).ToLocalTime();
            this.Description = details.Description;
            this.Details = details.Details;
            this.Due = utcDue.ToLocalTime();
            this.EndMessage = details.EndMessage;
            this.Name = details.Name;
            this.Public = details.Public;
            this.PathName = details.Id;
            this.OwnerEmail = details.OwnerEmail;
            this.SecondsToGo = DateTimeSupport.GetSecondsToGo(details.Due);
            this.StyleName = details.StyleName ?? "Default";

            if (details.HasImages)
            {
                BlobStorageReaderWriter blobReader = new BlobStorageReaderWriter();
                _imageUrls.AddRange(blobReader.GetBlobUrls(details.Id));
            }
        }

        [Required(ErrorMessage = "Please enter a name for the event", AllowEmptyStrings = false)]
        [Display(Name = "Name of the event")]
        public string Name { get; set; }

        [Key]
        [Display(Name = "URL of the event")]
        public string PathName { get; set; }

        [Required(ErrorMessage = "Please describe the event", AllowEmptyStrings = false)]
        [Display(Name = "Short description")]
        public string Description { get; set; }

        public string Details { get; set; }

        public string EndMessage { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress]
        [Display(Name = "Email address")]
        public string OwnerEmail { get; set; }

        public bool Public { get; set; }

        public bool AllowComments { get; set; }

        public DateTime Created { get; set; }

        public ICollection<string> ImageUrls => _imageUrls;

        public ICollection<string> ImageNames
        {
            get
            {
                return _imageUrls.Select(url => Path.GetFileName(url)).ToList();
            }
        }

        public ICollection<string> DeletedImages { get; set; }

        [Required(ErrorMessage = "Please enter when the event is due to happen")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        [Display(Name = "Due date")]
        public DateTime Due { get; set; }

        public int SecondsToGo { get; }

        public string StyleName { get; set; }

        public static SelectList Styles
        {
            get
            {
                return new SelectList(_styles);
                /*
                return new SelectList(_styles.Select(style => new SelectListItem
                {
                    Value = style,
                    Text = style
                }), "StyleName", "StyleName", StyleName);*/
            }
        }
    }
}
