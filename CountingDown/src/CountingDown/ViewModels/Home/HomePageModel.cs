﻿using System;
using System.Collections.Generic;

namespace CountingDown.ViewModels.Home
{
    public class HomePageModel
    {
        public IList<EventMoreInfo> RandomEvents { get; set; }

        public ICollection<EventBasicInfo> RecentEvents { get; set; }

        public ICollection<EventBasicInfo> EndingEvents { get; set; }

        public ICollection<EventBasicInfo> EndedEvents { get; set; }
    }

    public class EventBasicInfo
    {
        public string EventName { get; set; }
        public string EventId { get; set; }
    }

    public class EventMoreInfo : EventBasicInfo
    {
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public int RemainingTimeInSeconds { get; set; }
        public string DueDate { get; set; }
    }
}
