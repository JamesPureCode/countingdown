﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountingDown.Models
{
    [Serializable]
    public class EventDetails : Microsoft.Azure.Documents.Resource
    {
        public string Name { get; set; }

        public string OwnerEmail { get; set; }

        public string Description { get; set; }

        public string Details { get; set; }

        public string EndMessage { get; set; }

        public bool Public { get; set; }

        public bool AllowComments { get; set; }

        public string PasswordHsh { get; set; }

        public string Created { get; set; }

        public string Due { get; set; }

        public string StyleName { get; set; }

        public bool HasImages { get; set; }
    }
}
