﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountingDown.Models
{
    [Serializable]
    public class UserDetails
    {
        public UserDetails()
        {
        }

        public UserDetails(string email)
        {
            this.Email = email;
        }

        [System.ComponentModel.DataAnnotations.Key]
        public string Email { get; set; }

        public string Name { get; set; }

        public string Alias { get; set; }

        public string AdminPasswordHsh { get; set; }
    }
}
