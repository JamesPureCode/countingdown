﻿using System;
using System.Globalization;

namespace CountingDown
{
    public static class DateTimeSupport
    {
        public static DateTime FromString(string dateString)
        {
            DateTime dateTime;
            if (!DateTime.TryParseExact(dateString, "o", CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind, out dateTime))
            {
                dateTime = DateTime.Parse(dateString, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
            }
            return dateTime;
        }

        public static string ToString(DateTime dateTime)
        {
            if (dateTime.Kind != DateTimeKind.Utc)
            {
                dateTime = dateTime.ToUniversalTime();
            }
            return dateTime.ToString("o", CultureInfo.InvariantCulture);
        }

        public static string NowUtc()
        {
            return ToString(FixedUtcNow);
        }

        /// <summary>
        /// The clock on the Azure host seems to be wrong! So use this!
        /// </summary>
        public static DateTime FixedUtcNow
        {
            get
            {
                DateTime now = DateTime.UtcNow.AddHours(1d);
                return DateTime.SpecifyKind(now, DateTimeKind.Utc);
            }
        }

        public static int GetSecondsToGo(string due)
        {
            DateTime utcDue = FromString(due);
            double span = ((utcDue - FixedUtcNow).TotalSeconds);
            return (span < 0.0) ? 0 : (int)span;
        }
    }
}
