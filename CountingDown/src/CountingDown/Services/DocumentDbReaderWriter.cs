﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CountingDown.Models;
using CountingDown.ViewModels.Home;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace CountingDown.Services
{
	public sealed class DocumentDbReaderWriter : IDisposable
	{
		private const string Endpoint = "https://counting.documents.azure.com:443/";//"https://countingdownto.documents.azure.com:443/";
		private const string AuthKey = "X6bOfPNkWNORbmi3JktgTSDNM476RKXT5pnGaD07usPHlaCX59Jc1sKL9unasdamRiv5YjdlJEZ7LKxl6gtAmg==";//"YnNAhvh9xUwkrkXmPAlSHIQjabcqsKaaIT773zEoRA0dAnumYyC2PkXF9v4RfMJJyuh03HSbpebyrqZjzjQ0Ug==";
		private readonly static Uri DraftEventsLink = UriFactory.CreateDocumentCollectionUri("CountingDownDB", "DraftEvents");
		private readonly static Uri UsersLink = UriFactory.CreateDocumentCollectionUri("CountingDownDB", "Users");

		private readonly DocumentClient _client = new DocumentClient(new Uri(Endpoint), AuthKey);

		public async Task<UserDetails> SaveNewDraftEventAndReadUser(EventDetails details)
		{
			/*             
                    var database = new Database { Id = "CountingDownDB" };
                    database = await _client.CreateDatabaseAsync(database);

                    var events = new DocumentCollection { Id = "DraftEvents" };
                    events.IndexingPolicy = new IndexingPolicy(new RangeIndex(DataType.String) { Precision = -1 });
                    events = await _client.CreateDocumentCollectionAsync(database.SelfLink, events);

                    var users = new DocumentCollection { Id = "Users" };
                    users = await _client.CreateDocumentCollectionAsync(UriFactory.CreateDatabaseUri("CountingDownDB"), users);

            */
			string password = null;

			EventDetails existingDoc = _client.CreateDocumentQuery<EventDetails>(DraftEventsLink).Where(d => d.Id == details.Id).AsEnumerable().FirstOrDefault();

			ResourceResponse<Document> response;
			if (existingDoc == null)
			{
				if (!details.Public)
				{
					details.PasswordHsh = GenerateNewPassword(out password);
				}

				response = await _client.CreateDocumentAsync(DraftEventsLink, details);
				if (response.StatusCode != System.Net.HttpStatusCode.Created)
				{
					return null;
				}
			}
			else
			{
				details.PasswordHsh = existingDoc.PasswordHsh;
				if (!details.Public && string.IsNullOrEmpty(details.PasswordHsh))
				{
					details.PasswordHsh = GenerateNewPassword(out password);
				}

				response = await _client.ReplaceDocumentAsync(existingDoc.SelfLink, details);
				if (response.StatusCode != System.Net.HttpStatusCode.OK)
				{
					return null;
				}
			}


			if (!string.IsNullOrEmpty(password))
			{
				// todo: need to tell the user what this is!!!!!
			}

			// If the user is already registered get their details:
			return ReadUserDetails(details.OwnerEmail);
		}

		private string GenerateNewPassword(out string password)
		{
			password = Password.Generate(10, 2);
			return Password.CreateMD5(password);
		}

		public UserDetails ReadUserDetails(string email)
		{
			UserDetails user = _client.CreateDocumentQuery<UserDetails>(UsersLink,
				$"SELECT * FROM Users u WHERE u.Email = '{email}'").AsEnumerable().FirstOrDefault();
			return user ?? new UserDetails(email);
		}

		public EventDetails ReadDraftEvent(string eventId)
		{
			return _client.CreateDocumentQuery<EventDetails>(DraftEventsLink,
				$"SELECT * FROM DraftEvents e WHERE e.id = '{eventId}'").AsEnumerable().FirstOrDefault();
		}

		public async Task<bool> DeleteDraftEvent(string eventId)
		{
			Uri documentLink = UriFactory.CreateDocumentUri("CountingDownDB", "DraftEvents", eventId);
			ResourceResponse<Document> response = await _client.DeleteDocumentAsync(documentLink);
			return (response.StatusCode == System.Net.HttpStatusCode.NoContent);
		}

		public ICollection<EventBasicInfo> GetLatestEvents()
		{
			//             return new List<Tuple<string, string>>();

			var dtls = _client.CreateDocumentQuery<EventDetails>(DraftEventsLink,
				"SELECT TOP 5 * FROM DraftEvents e ORDER BY e.Created DESC");

			return dtls.AsEnumerable().Select(e => new EventBasicInfo
			{ EventName = e.Name, EventId = e.Id }).ToList();
		}

		public ICollection<EventBasicInfo> GetEndingEvents()
		{
			string now = DateTimeSupport.NowUtc();

			var dtls = _client.CreateDocumentQuery<EventDetails>(DraftEventsLink,
				$"SELECT TOP 5 * FROM DraftEvents e WHERE e.Due > '{now}' ORDER BY e.Due");

			return dtls.AsEnumerable().Select(e => new EventBasicInfo
			{ EventName = e.Name, EventId = e.Id }).ToList();
		}

		public ICollection<EventBasicInfo> GetEndedEvents()
		{
			string now = DateTimeSupport.NowUtc();

			var dtls = _client.CreateDocumentQuery<EventDetails>(DraftEventsLink,
				$"SELECT TOP 5 * FROM DraftEvents e WHERE e.Due < '{now}' ORDER BY e.Due DESC");

			return dtls.AsEnumerable().Select(e => new EventBasicInfo
			{ EventName = e.Name, EventId = e.Id }).ToList();
		}

		public IList<EventMoreInfo> GetSomeRandomEvents(int count, BlobStorageReaderWriter blobReader)
		{
			Random rnd = new Random();
			var randomEvents = _client.CreateDocumentQuery<EventDetails>(DraftEventsLink,
				"SELECT * FROM DraftEvents e WHERE e.HasImages = true", new FeedOptions { MaxItemCount = 1000 }).AsEnumerable().OrderBy(x => rnd.Next()).Take(count);
			return randomEvents.Select(e => new EventMoreInfo
			{
				EventName = e.Name,
				EventId = e.Id,
				ImagePath = blobReader.GetRandomBlobUrl(e.Id),
				Description = e.Description,
				DueDate = e.Due,
				RemainingTimeInSeconds = DateTimeSupport.GetSecondsToGo(e.Due)
			}).ToList();
		}

		public void Dispose()
		{
			_client.Dispose();
		}
	}
}
