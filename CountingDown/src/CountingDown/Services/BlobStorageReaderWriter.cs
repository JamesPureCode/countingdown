﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNet.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Net.Http.Headers;

namespace CountingDown.Services
{
    public struct ImageUploadResult
    {
        public bool Success;
        public string Error;
    }

    public class BlobStorageReaderWriter
    {

        //        https://playing2819.blob.core.windows.net/
        //        private static CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=countingdownto;AccountKey=H11SSuz54PMcnrm7PPHJDl93Y+J2stxnNMac51N76vGcDfX7IMUQUMDHQJa9kOg1o+rT2w3nS/9jcQHcYk21UQ==");
        private static CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=playing2819;AccountKey=D9gy90Le+I0ZeFtDkOyfNGixt80FPGMuxnenpheyJR+cpCkhZ4OUv+9fEjGXZafW8an7YSTgIiWmaymBoYs/FQ==");

        private readonly CloudBlobClient _blobClient;
        private readonly CloudBlobContainer _container;

        public BlobStorageReaderWriter()
        {
            _blobClient = storageAccount.CreateCloudBlobClient();
            _container = _blobClient.GetContainerReference("img");
        }

        private async Task<ImageUploadResult> UploadImage(Image image, string blobName)
        {
            CloudBlockBlob blockBlob = _container.GetBlockBlobReference(blobName);

            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Jpeg);
                byte[] imgData = ms.GetBuffer();
                await blockBlob.UploadFromByteArrayAsync(imgData, 0, imgData.Length);
                /* We use a byte array above because
                 * await blockBlob.UploadFromStreamAsync(fileStream);
                 * is not reliable!
                 * See: https://github.com/Azure/azure-storage-net/issues/202
                 */
            }
            return new ImageUploadResult
            {
                Success = true
            };
        }

        public async Task<ImageUploadResult[]> ValidateAndUploadImages(IEnumerable<IFormFile> files, string eventId)
        {
            return await Task.WhenAll(files.Select(f => ValidateAndUploadImage(f, eventId)));
        }

        private async Task<ImageUploadResult> ValidateAndUploadImage(IFormFile file, string eventId)
        {
            try
            {
                if (file.Length < 10 || file.Length > 5242880)
                {
                    return new ImageUploadResult
                    {
                        Success = false,
                        Error = "Invalid file size."
                    };
                }

                string imageName;
                ContentDispositionHeaderValue fileHeader;
                if (ContentDispositionHeaderValue.TryParse(file.ContentDisposition, out fileHeader))
                {
                    imageName = Path.GetFileNameWithoutExtension(fileHeader.FileName.Trim('"'));
                }
                else
                {
                    imageName = "image";
                }

                using (var fileStream = file.OpenReadStream())
                {
                    Image image = Image.FromStream(fileStream, true, true);

                    int newWidth = image.Width;
                    int newHeight = image.Height;

                    if (newWidth > 720)
                    {
                        newWidth = 720;
                        newHeight = (int)((720d / image.Width) * image.Height);
                    }
                    if (newHeight > 800)
                    {
                        newHeight = 800;
                        newWidth = (int)((800d / image.Height) * image.Width);
                    }
                    if (newWidth != image.Width || newHeight != image.Height)
                    {
                        using (var bmp = new Bitmap(image, newWidth, newHeight))
                        {
                            return await UploadImage(bmp, $"{eventId}/{imageName}");
                        }
                    }
                    return await UploadImage(image, $"{eventId}/{imageName}");
                }
            }
            catch (ArgumentException)
            {
                return new ImageUploadResult
                {
                    Success = false,
                    Error = "Invalid image file."
                };
            }
        }

        public async void TryDeleteImages(string eventId)
        {
            var blobs = _container.ListBlobs(eventId, true);
            await Task.WhenAll(blobs.Select(blob => _container.GetBlockBlobReference(((CloudBlockBlob)blob).Name).DeleteIfExistsAsync()));
        }

        public async void TryDeleteImages(string eventId, ICollection<string> imageNames)
        {
            await Task.WhenAll(imageNames.Select(imageName => _container.GetBlockBlobReference($"{eventId}/{imageName}").DeleteIfExistsAsync()));
        }

        public IEnumerable<string> GetBlobUrls(string eventId)
        {
            List<IListBlobItem> blobs = _container.ListBlobs(eventId, true).ToList();
            if (blobs.Count > 0)
            {
                return blobs.Select(b => b.Uri.AbsoluteUri).ToList();
            }
            return Enumerable.Empty<string>();
        }

        public string GetRandomBlobUrl(string eventId)
        {
            List<IListBlobItem> blobs = _container.ListBlobs(eventId, true).ToList();
            if (blobs.Count > 1)
            {
                Random rnd = new Random();
                return blobs.OrderBy(i => rnd.Next()).Select(b => b.Uri.AbsoluteUri).FirstOrDefault();
            }
            if (blobs.Count > 0)
            {
                return blobs[0].Uri.AbsoluteUri;
            }
            return null;
        }

        public bool HasImages(string eventId)
        {
            return _container.ListBlobs(eventId, true).Any();
        }
    }
}
