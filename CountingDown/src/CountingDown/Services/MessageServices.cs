﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace CountingDown.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        public Task SendEmailAsync(string emailAddress, string subject, string message)
        {
            SendGridAPIClient sg = new SendGridAPIClient("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");

            Email to = new Email(emailAddress);
            Email from = new Email("admin@countingdown.to", "CountingDown.to");
            Content content = new Content("text/plain", message);
            Mail mail = new Mail(from, subject, to, content);

            return Task.Run(() => sg.client.mail.send.post(requestBody: mail.Get()));
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
