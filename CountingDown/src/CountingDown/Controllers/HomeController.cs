﻿using System;
using CountingDown.Models;
using CountingDown.Services;
using CountingDown.ViewModels.Account;
using CountingDown.ViewModels.Home;
using Microsoft.AspNet.Mvc;

namespace CountingDown.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            HomePageModel model = new HomePageModel();
            using (DocumentDbReaderWriter reader = new DocumentDbReaderWriter())
            {
                BlobStorageReaderWriter blobReader = new BlobStorageReaderWriter();
                model.RandomEvents = reader.GetSomeRandomEvents(4, blobReader);
                model.RecentEvents = reader.GetLatestEvents();
                model.EndingEvents = reader.GetEndingEvents();
                model.EndedEvents = reader.GetEndedEvents();
            }
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Event()
        {
            return View();
        }

        private EventViewModel ConvertToEventViewModel(EventDetails details)
        {
            if (details == null || string.IsNullOrEmpty(details.Name))
                return null;

            return new EventViewModel(details);
        }

        [Route("{id}")]
        public IActionResult ViewEvent(string id)
        {
            EventViewModel viewModel;
            using (DocumentDbReaderWriter reader = new DocumentDbReaderWriter())
            {
                viewModel = ConvertToEventViewModel(reader.ReadDraftEvent(id));
            }
            if (viewModel == null)
            {
                return RedirectToAction("NewEvent", "Account");
            }
            return View(viewModel);
        }
    }
}
