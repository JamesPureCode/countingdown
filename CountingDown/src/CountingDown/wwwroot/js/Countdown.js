﻿var _second = 1000;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour * 24;
var _week = _day * 7;
var _month = _week * 4;
var _year = _month * 12;
var timer;

function showRemaining() {
    if (distance < 0) {
        clearInterval(timer);
        document.getElementById('countdown').innerHTML = 'EXPIRED!';
        return;
    }

    years = Math.floor(distance / _year);
    months = Math.floor((distance % _year) / _month);
    weeks = Math.floor((distance % _month) / _week);
    days = Math.floor((distance % _week) / _day);
    hours = Math.floor((distance % _day) / _hour);
    minutes = Math.floor((distance % _hour) / _minute);
    seconds = Math.floor((distance % _minute) / _second);

    years = ('00'+years).slice(-2);
    months = ('00'+months).slice(-2);
    weeks = ('00'+weeks).slice(-2);
    days = ('00'+days).slice(-2);
    hours = ('00'+hours).slice(-2);
    minutes = ('00'+minutes).slice(-2);
    seconds = ('00'+seconds).slice(-2);

    // Hide styling if any of these three are zero
    if (years === "00")
    {
        if (document.getElementById('countdown_year'))
        {
            document.getElementById('countdown_year').parentElement.outerHTML = '';
        }

        if (months === "00")
        {
            if (document.getElementById('countdown_month'))
            {
                document.getElementById('countdown_month').parentElement.outerHTML='';
            }

            if (weeks === "00")
            {
                if (document.getElementById('countdown_week'))
                {
                    document.getElementById('countdown_week').parentElement.outerHTML='';
                }
            }
            else
            {
                document.getElementById('countdown_week').innerHTML = weeks.slice(0,1);
                document.getElementById('countdown_week2').innerHTML = weeks.slice(1,2);
            }
        }
        else
        {
            document.getElementById('countdown_month').innerHTML = months.slice(0,1);
            document.getElementById('countdown_month2').innerHTML = months.slice(1,2);
            document.getElementById('countdown_week').innerHTML = weeks.slice(0,1);
            document.getElementById('countdown_week2').innerHTML = weeks.slice(1,2);
        }
    }
    else
    {
        document.getElementById('countdown_year').innerHTML = years.slice(0,1);
        document.getElementById('countdown_year2').innerHTML = years.slice(1,2);
        document.getElementById('countdown_month').innerHTML = months.slice(0,1);
        document.getElementById('countdown_month2').innerHTML = months.slice(1,2);
        document.getElementById('countdown_week').innerHTML = weeks.slice(0,1);
        document.getElementById('countdown_week2').innerHTML = weeks.slice(1,2);
    }

    document.getElementById('countdown_day').innerHTML = days.slice(0,1);
    document.getElementById('countdown_day2').innerHTML = days.slice(1,2);
    document.getElementById('countdown_hour').innerHTML = hours.slice(0,1);
    document.getElementById('countdown_hour2').innerHTML = hours.slice(1,2);
    document.getElementById('countdown_minute').innerHTML = minutes.slice(0,1);
    document.getElementById('countdown_minute2').innerHTML = minutes.slice(1,2);
    document.getElementById('countdown_second').innerHTML = seconds.slice(0,1);
    document.getElementById('countdown_second2').innerHTML = seconds.slice(1,2);

    distance = distance - 1000;
}
showRemaining();
timer = setInterval(showRemaining, 1000);